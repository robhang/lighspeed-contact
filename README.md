# lightspeed contact

## Build Setup

``` bash
# install json-server to mock api calls (this proof of concept will NOT work if we don't mock api calls)
npm install -g json-server

# start mock api server
cd lighspeed-contact
json-server --watch db.json

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# serve as production bundle after building
cd dist
## use any type of local server available to serve files! This won't work if serving over file:/// :(
python -m SimpleHTTPServer

# run all tests
npm test
```