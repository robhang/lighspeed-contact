import { ContactsService } from '@/core/services/contacts-service';

const contactDeleteMixins = {
    methods: {
        handleDelete() {
            if (window.confirm('Delete this contact?')) {
                ContactsService.deleteContact({ contact: this.contact }).then(() => {
                    this.$store.dispatch('getContacts');
                });
            }
        }
    }
};

export { contactDeleteMixins };
