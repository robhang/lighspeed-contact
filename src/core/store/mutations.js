const mutations = {
    setContacts(state, { contacts }) {
        state.contacts = contacts;
    },

    setCurrentContact(state, { currentContact }) {
        state.currentContact = currentContact;
    },

    setSearchedContacts(state, { searchedContacts }) {
        state.searchedContacts = searchedContacts;
    },

    setQuery(state, { query }) {
        state.query = query;
    }
};

export { mutations };
