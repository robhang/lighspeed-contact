import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
chai.use(sinonChai);

import { actions } from './actions';
import { ContactsService } from '../services/contacts-service';

describe('actions', () => {
    describe('getContacts', () => {
        const { getContacts } = actions;
        const commit = sinon.spy();
        const state = {};
        let ContactsServiceSpy;

        beforeEach(() => {
            ContactsServiceSpy = sinon
                .stub(ContactsService, 'getContacts')
                .returns(Promise.resolve({ data: [{ test: 'test' }] }));

            getContacts({ commit, state });
        });

        afterEach(() => {
            ContactsServiceSpy.restore();
        });

        it('should get data using ContactsService ', () => {
            expect(ContactsServiceSpy).to.have.been.called;
        });
    });
});
