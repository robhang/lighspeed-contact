import Fuse from 'fuse.js';
import { ContactsService } from '../services/contacts-service';

const fuseOptions = {
    keys: ['name', 'job_title', 'emails', 'address']
};

const actions = {
    getContacts({ commit }) {
        ContactsService.getContacts().then(({ data }) => {
            commit('setContacts', {
                contacts: data
            });
        });
    },

    getCurrentContact({ commit }, { id }) {
        ContactsService.getContact(id).then(({ data }) => {
            commit('setCurrentContact', {
                currentContact: data
            });
        });
    },

    search({ commit, state }) {
        const fuse = new Fuse(state.contacts, fuseOptions);
        commit('setSearchedContacts', {
            searchedContacts: fuse.search(state.query)
        });
    }
};

export { actions };
