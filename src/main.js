import Vue from 'vue';
import { store } from './core/store';
import { router } from './core/router';
import VueMq from 'vue-mq';
import App from './App';

require('./style/main.scss');

Vue.config.productionTip = false;
Vue.use(VueMq, {
    breakpoints: {
        mobile: 550,
        desktop: Infinity
    }
});
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: {
        App
    }
});
