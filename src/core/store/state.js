const state = {
    contacts: [],
    currentContact: {},
    searchedContacts: [],
    query: ''
};

export { state };
