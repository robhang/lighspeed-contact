import axios from 'axios';
import { contactModel } from '../models/contact-model';
import { domainPrefix } from './constants';

/**
 * Service responsible for CRUD operations for the Contact model.
 *
 */
const ContactsService = {
    getContacts() {
        return axios.get(`${domainPrefix}/contacts`);
    },

    getContact(id) {
        return axios.get(`${domainPrefix}/contacts/${id}`);
    },

    getEmptyContact({ options = undefined } = {}) {
        return new contactModel({ options });
    },

    saveContact({ contact }) {
        return axios.put(`${domainPrefix}/contacts/${contact.id}`, {
            ...contact
        });
    },

    saveNewContact({ contact }) {
        return axios.post(`${domainPrefix}/contacts/`, {
            ...contact
        });
    },

    deleteContact({ contact }) {
        return axios.delete(`${domainPrefix}/contacts/${contact.id}`);
    }
};

export { ContactsService };
