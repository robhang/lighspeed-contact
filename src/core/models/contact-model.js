class contactModel {
    constructor({ options = undefined } = {}) {
        this.name = options.name || 'No results found';
        this.picture = options.picture || { thumbnail: null };
        this.emails = options.emails || [];
        this.phones = options.phones || [];
        this.address = options.address || {
            street: '',
            city: '',
            state: '',
            country: '',
            postal: ''
        };
        this.job_title = options.job_title || null;
    }
}

export { contactModel };
