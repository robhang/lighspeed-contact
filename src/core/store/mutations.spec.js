import { expect } from 'chai';
import { mutations } from './mutations';

describe('mutations', () => {
    describe('setContacts', () => {
        const { setContacts } = mutations;

        it('should set an array of contacts to the state', () => {
            const state = {
                contacts: []
            };

            setContacts(state, { contacts: [{ name: 'test' }] });

            expect(state.contacts).to.have.lengthOf(1);
        });
    });
});
