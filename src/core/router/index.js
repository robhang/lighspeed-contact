import Vue from 'vue';
import Router from 'vue-router';
import dashboard from '@/components/dashboard/dashboard.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: dashboard
        },
        {
            path: '/contact/new',
            name: 'newContact',
            component: () => import('@/components/contact/contact.vue')
        },
        {
            path: '/contact/:id',
            name: 'contact',
            component: () => import('@/components/contact/contact.vue')
        }
    ]
});
export { router };
